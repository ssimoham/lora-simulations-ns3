for distance in 100
do
    i=$1
    path="../lorawan/tracking/$distance-m"
    mkdir -p "${path}/txt" "${path}/parsed" "${path}/csv" "${path}/loss"
    rm "${path}/loss/losses.csv"
    while [ $i -le $2 ]
    do
        num=$(( $i ))
        echo "NSta=$num" > "${path}/csv/$num.csv"

        ./waf --run "scratch/lora-tracking.cc --nDevices=$num --radius=$distance" 2> "${path}/txt/$num.txt" > "${path}/loss/$num.txt"
        #cat "${path}/txt/$num.txt" | grep -e "GatewayLorawanMac:Receive()" -e "EndDeviceLorawanMac:Send(" > "${path}/parsed/$num.txt"
        #python3 get_latencies.py "${path}/parsed/$num.txt" "${path}/csv/$num.csv"
        
        python3 get_loss.py "${path}/loss/$num.txt" "${path}/loss/losses.csv" $num

        ((i=i+1000)) #Step = 50
    done
done
