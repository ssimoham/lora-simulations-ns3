for i in 100,230 3000,230 4000,123 5000,59 5500,59 6000,59;
    do IFS=",";
    set -- $i;
    declare -a payloadSize=$2
    declare -a distance=$1
    declare -a j=1

    path="../lorawan/times/$distance-m"
    mkdir -p "${path}/txt" "${path}/parsed" "${path}/csv"
    rm "${path}/csv/times.csv"
    #num=$(( $i ))
    while [ $j -le $payloadSize ]
    do

        ./waf --run "scratch/lora-overload.cc --nDevices=1 --payloadSize=$j --radius=$distance" 2> "${path}/txt/$j.txt"
        cat "${path}/txt/$j.txt" | grep -e "LoraPhy:GetOnAirTime(): Total time = " > "${path}/parsed/$j.txt"

        python3 get_time.py "${path}/parsed/$j.txt" "${path}/csv/times.csv" $j        

        ((j=j+5)) #Step = 50
    done


done
