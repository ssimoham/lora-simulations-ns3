declare -a nDevices=$1

for i in 100,230,30 3000,230,60 4000,123,60 5000,59,70 5500,59,130 6000,59,260;
do IFS=",";
    set -- $i;
    declare -a payloadSize=$2
    declare -a distance=$1
    declare -a period=$3

    echo $payloadSize $distance $period

    for k in {1..10};
    do
        seed=$(($k))
        declare -a j=1
        path="../lorawan/overload/$distance-m"
        mkdir -p "${path}/txt" "${path}/parsed" "${path}/csv" "${path}/loss-$seed"
        rm "${path}/loss-$seed/losses.csv"
        while [ $j -le $nDevices ]
        do
            num=$(( $j ))
            echo "NSta=$num" > "${path}/csv/$num.csv"

            ./waf --run "scratch/lora-overload.cc --appPeriod=$period --seed=$seed --payloadSize=$payloadSize --nDevices=$num --radius=$distance" 2> "${path}/txt/$num.txt" > "${path}/loss-$seed/$num.txt"
            #cat "${path}/txt/$num.txt" | grep -e "GatewayLorawanMac:Receive()" -e "EndDeviceLorawanMac:Send(" > "${path}/parsed/$num.txt"
            #python3 get_latencies.py "${path}/parsed/$num.txt" "${path}/csv/$num.csv"

            python3 get_loss.py "${path}/loss-$seed/$num.txt" "${path}/loss-$seed/losses.csv" $num

            if [[ $j -eq 1 ]]
            then
                ((j=j+9)) #Step = 100
            else
                ((j=j+10))
            fi
        done
    done
done