import csv
import sys

filepath = sys.argv[1]
output = sys.argv[2]
number = sys.argv[3]

dict = []

with open(filepath) as fp:
   line = fp.readline()
   while line:
       words = line.split()
       time = float(words[6])
       print(time)
       with open(output, 'a', newline='') as file:
           write = csv.writer(file)
           write.writerow([number, time])
           break
