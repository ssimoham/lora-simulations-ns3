#! /usr/bin/env python3

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3-dev-lora-overload-debug', 'build/scratch/ns3-dev-scratch-simulator-debug', 'build/scratch/subdir/ns3-dev-subdir-debug', 'build/scratch/ns3-dev-lora-telemetry-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

